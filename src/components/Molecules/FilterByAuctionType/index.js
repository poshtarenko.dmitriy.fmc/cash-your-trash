import React, { Component } from "react";
import store from "../../../redux/store";
import Icon from "../../Atoms/Icons";
import "./style.scss";

export class FilterByAuctionType extends Component {
  /**
   * Filter by Auction type
   *
   * @param {object} text - show/hide filter (advert, auction, false)
   * @param {string} auctionsType - current auction type
   * @return {string} changeAuctionsType - return type of auction (owner or participant)
   */

  constructor(props) {
    super(props);

    this.state = {};
  }

  toggleCreated = () => {
    this.props.changeAuctionsType("owner");
  };

  toggleInvolved = () => {
    this.props.changeAuctionsType("participant");
  };

  render() {
    return (
      <div className="filter-by-type">
        <button
          onClick={this.toggleCreated}
          className={this.props.auctionsType === "owner" ? "active" : ""}
        >
          <Icon name="icons-buy" width="14" height="14" />
          <span>{this.props.text.create}</span>
        </button>
        <button
          onClick={this.toggleInvolved}
          className={this.props.auctionsType === "participant" ? "active" : ""}
        >
          <Icon name="icons-sell" width="14" height="14" />
          <span>
            {store.getState().myAdverts.auctionStatus === "sold"
              ? this.props.text.participated
              : this.props.text.participating}
          </span>
        </button>
      </div>
    );
  }
}

export default FilterByAuctionType;
