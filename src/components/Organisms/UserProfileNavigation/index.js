import React, { Component } from "react";
import FilterByAdvertType from "../../Molecules/FilterByAdvertType";
import LanguageConfig from "../../../config/translation.json";
import { Pagination } from "semantic-ui-react";
import { browserDetected } from "../../../tools/browserDetected";
import "./style.scss";
import FilterByAuctionType from "../../Molecules/FilterByAuctionType";

export class UserProfileNavigation extends Component {
  /**
   * Navigation panel in User Profile
   *
   * @param {string} filter - show/hide filter (advert, auction, false)
   * @param {boolean} pagination - show/hide pagination
   * @param {boolean} bottom - add bottom padding if true
   * @param {number} total - products count
   * @param {number} page - page number
   * @param {number} deal - type of deal
   * @param {string} language - current language
   * @returns {number} handlePaginationChange - return active page
   * @returns {number} changeAdvertsDealType - return type of deal (1 or 2)
   * @returns {string} changeAuctionsType - return type of auction (owner or participant)
   */

  constructor(props) {
    super(props);

    this.state = {};
  }

  scrollToTop() {
    let wrapper = document.querySelector("body");
    let main_page = document.querySelector(".user_page_container");
    let scroll_top_value =
      main_page.offsetTop - document.querySelector("#header").offsetHeight;
    if (browserDetected.isChrome() || browserDetected.isOpera()) {
      wrapper.scrollTo({ top: scroll_top_value, behavior: "smooth" });
      window.scrollTo({ top: scroll_top_value, behavior: "smooth" });
    } else {
      wrapper.scrollTop = scroll_top_value;
      window.scrollTo({ top: scroll_top_value });
    }
  }

  handlePaginationChange = (e, { activePage }) => {
    this.scrollToTop();
    this.props.handlePaginationChange(activePage);
  };

  changeAdvertsDealType = type => {
    this.props.changeAdvertsDealType(type);
  };

  changeAuctionsType = type => {
    this.props.changeAuctionsType(type);
  };

  render() {
    const pagesCount = Number(this.props.total) / 24;
    return (
      <div
        className={`user-profile-navigation ${
          this.props.bottom ? "bottom" : ""
        } ${
          !this.props.filter && this.props.pagination ? "only-pagination" : ""
        }`}
      >
        {this.props.filter === "advert" && (
          <FilterByAdvertType
            text={LanguageConfig[this.props.language]["filtersBy"]}
            changeAdvertsDealType={this.changeAdvertsDealType}
            deal={this.props.deal}
          />
        )}
        {this.props.filter === "auction" && (
          <FilterByAuctionType
            text={LanguageConfig[this.props.language]["filtersBy"]}
            changeAuctionsType={this.changeAuctionsType}
            auctionsType={this.props.auctionsType}
          />
        )}
        {this.props.pagination &&
          (pagesCount > 1 ? (
            <Pagination
              boundaryRange={0}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={1}
              activePage={this.props.page}
              onPageChange={this.handlePaginationChange}
              totalPages={Math.ceil(pagesCount)}
            />
          ) : (
            <div></div>
          ))}
      </div>
    );
  }
}

export default UserProfileNavigation;
