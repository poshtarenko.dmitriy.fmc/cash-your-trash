import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Products from "../../Products";
import ProductLable from "../../../Molecules/ProductLable";
import SimpleModal from "../../../Atoms/SimpleModal";
import myAdverts from "../../../../actions/myAdverts";
import LoaderStyled from "../../../Atoms/LoaderStyled";
import LanguageConfig from "../../../../config/translation.json";
import GeneralButton from "../../../Atoms/GeneralButton";
import ShowMoreButton from "../../../Atoms/ShowMoreButton";
import UserProfileNavigation from "../../UserProfileNavigation";
import ConfirmModal from "../../../Molecules/ConfirmModal";
import "./style.scss";

export class UserAdverts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adRemoved: false,
      isOpenModal: false,
      isOpenModalConfirm: false,
      removedId: null,
      changedId: null
    };
  }

  // On component update life circle
  componentWillReceiveProps(newProps) {
    if (newProps.user.id !== this.props.user.id) {
      myAdverts.getUserAdverts({
        id: newProps.user.id,
        page: 1,
        limit: 24,
        status: this.props.myAdverts.status,
        deal: 1
      });
    }
  }

  // On component did mount life circle
  componentDidMount() {
    if (this.props.user.id) {
      myAdverts.getUserAdverts({
        id: this.props.user.id,
        page: 1,
        limit: 24,
        status: this.props.myAdverts.status,
        deal: 1
      });
    }
  }

  closeConfirmModal = () => {
    this.setState({
      isOpenModalConfirm: false
    });
  };

  closeModal = () => {
    this.setState({
      isOpenModal: false
    });
  };

  // Stage advert id before removing
  removeAdvert = id => {
    this.setState({
      isOpenModal: true,
      removedId: id
    });
  };

  // Confirm modal on remove advert
  removeAdvertConfirm = async () => {
    const response = await myAdverts.removeAdvert(
      this.state.removedId,
      this.props.user.token,
      this.props.myAdverts.list
    );
    if (response.status === 200) {
      this.setState({
        isOpenModal: false,
        removedId: null,
        adRemoved: true
      });
      const self = this;
      setTimeout(() => {
        self.setState({
          adRemoved: false
        });
      }, 3000);
    }
  };

  // Change pagination handler
  handlePaginationChange = activePage => {
    myAdverts.getUserAdverts({
      id: this.props.user.id,
      page: activePage,
      limit: 24,
      status: this.props.myAdverts.status,
      deal: this.props.myAdverts.advertsDealType
    });
  };

  // Chenge deal type handler
  changeAdvertsDealType = type => {
    myAdverts.getUserAdverts({
      id: this.props.user.id,
      page: 1,
      limit: 24,
      status: this.props.myAdverts.status,
      deal: type
    });
  };

  // Change advert status by advert id
  changeAdvertStatus = id => {
    this.setState({
      isOpenModalConfirm: true,
      changedId: id
    });
  };

  // Confirm modal handler
  changeStatusHandler = () => {
    myAdverts.changeAdvertStatus({ status: "sold" }, this.state.changedId);
    this.setState({
      isOpenModalConfirm: false
    });
  };

  // Get more adverts (mobile pagination)
  async getMoreContent(context) {
    const adverts = context.props.myAdverts;
    adverts.page++;
    await myAdverts.getUserAdverts({
      id: context.props.user.id,
      page: adverts.page,
      limit: 24,
      status: context.props.myAdverts.status,
      deal: this.props.myAdverts.advertsDealType,
      mobile: true
    });
  }

  // Main component content
  addContent = () => {
    return (
      <React.Fragment>
        <UserProfileNavigation
          language={this.props.app.language}
          filter={"advert"}
          pagination={true}
          total={this.props.myAdverts.total}
          page={this.props.myAdverts.page}
          deal={this.props.myAdverts.advertsDealType}
          handlePaginationChange={this.handlePaginationChange}
          changeAdvertsDealType={this.changeAdvertsDealType}
        />
        <Products
          type={"userAdvert"}
          linkType={"user"}
          sizeType={this.props.sizeTypes}
          language={this.props.app.language}
          products={this.props.myAdverts.list}
          nav={true}
          history={this.props.history}
          removeAdvert={this.removeAdvert}
          changeAdvertStatus={this.changeAdvertStatus}
        />
        <UserProfileNavigation
          language={this.props.app.language}
          filter={false}
          pagination={true}
          bottom={true}
          total={this.props.myAdverts.total}
          page={this.props.myAdverts.page}
          deal={this.props.myAdverts.advertsDealType}
          handlePaginationChange={this.handlePaginationChange}
          changeAdvertsDealType={this.changeAdvertsDealType}
        />
        {Number(this.props.myAdverts.total) / 24 > 1 ? (
          <ShowMoreButton
            totalAdverts={this.props.myAdverts.total}
            currentPage={this.props.myAdverts.page}
            getMoreContent={() => this.getMoreContent(this)}
          />
        ) : (
          <div></div>
        )}
        <div className={this.state.adRemoved ? "notation active" : "notation"}>
          {LanguageConfig[this.props.app.language]["editAdvert"]["remove"]}
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <ProductLable
          name={
            LanguageConfig[this.props.app.language]["userProfileLabels"][
              "myAds"
            ]
          }
          count={0}
          close={false}
        />
        <div className="products-wrap">
          {!this.props.app.loaderStyled ? (
            this.addContent()
          ) : (
            <LoaderStyled show={this.props.app.loaderStyled} />
          )}
        </div>
        <SimpleModal
          isOpenModal={this.state.isOpenModal}
          closeHandler={this.closeModal}
          className="simple-modal"
          closeIcon={true}
        >
          <p className="header-modal">
            {
              LanguageConfig[this.props.app.language]["editAdvert"][
                "removeQuest"
              ]
            }
          </p>
          <div className="manage">
            <GeneralButton clicked={this.closeModal}>
              {LanguageConfig[this.props.app.language]["cancel"]}
            </GeneralButton>
            <GeneralButton
              classList="confirm"
              clicked={this.removeAdvertConfirm}
            >
              {LanguageConfig[this.props.app.language]["confirm"]}
            </GeneralButton>
          </div>
        </SimpleModal>
        <ConfirmModal
          title={
            LanguageConfig[this.props.app.language]["status"]["statusTitle"]
          }
          description={
            LanguageConfig[this.props.app.language]["status"][
              "statusDescription"
            ]
          }
          isOpenModal={this.state.isOpenModalConfirm}
          confirmHandler={this.changeStatusHandler}
          closeHandler={this.closeConfirmModal}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    app: state.app,
    home: state.home,
    user: state.user,
    myAdverts: state.myAdverts,
    sizeTypes: state.advert.sizeTypes
  };
};

export default connect(mapStateToProps)(withRouter(UserAdverts));
