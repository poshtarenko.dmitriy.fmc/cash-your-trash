import React, { Component } from "react";
import store from "../../../redux/store";
import "./style.scss";
import { app } from "../../../actions/app";

export class ShowMoreButton extends Component {
  /**
   * Show more adverts button
   *
   * @param {number} totalAdverts - total count of adverts
   * @param {number} currentPage - current pagination page
   * @param {function} getMoreContent - get adverts request
   */

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    app.showMoreButtonShow(true);
    if (
      Number(this.props.currentPage) ===
      Math.ceil(Number(this.props.totalAdverts) / 24)
    ) {
      app.showMoreButtonShow(false);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.totalAdverts !== this.props.totalAdverts) {
      if (
        Number(this.props.currentPage) ===
        Math.ceil(Number(this.props.totalAdverts) / 24)
      ) {
        app.showMoreButtonShow(false);
      } else {
        app.showMoreButtonShow(true);
      }
    }
  }

  async clickHandler(context) {
    if (
      Number(this.props.currentPage) <
      Math.ceil(Number(this.props.totalAdverts) / 24)
    ) {
      app.showMoreButtonShow(true);
      app.showMoreButtonLoading(true);
      await this.props.getMoreContent();
      app.showMoreButtonLoading(false);
      if (
        Number(this.props.currentPage) ===
        Math.ceil(Number(this.props.totalAdverts) / 24)
      ) {
        app.showMoreButtonShow(false);
      }
    }
  }

  render() {
    return (
      <button
        onClick={() => this.clickHandler(this)}
        className={
          "show-more-button " +
          (store.getState().app.buttonShowMore ? "simple " : "hide ") +
          (store.getState().app.buttonShowMoreLoading ? "loading" : "")
        }
      >
        Показать еще
      </button>
    );
  }
}

export default ShowMoreButton;
