# Cash Your Trash

Cash Your Trash is a project that was created to improve the environment. It`s a marketplace where you can sell 
waste production and other recyclable trash. You can use the usual method of sale or auction goods. This is a preview of real project which contains four components.

## Requirements

*  NPM v5.6 or higher
*  Node v5.10 or higher

## Installation

1.  Clone a git repository.
2.  Run in your project folder command `npm install`.
3.  Run command `npm run start`.

## Project folder tree
```
root
├── public
├── src
│   ├── actions
│   ├── api
│   ├── assets
│   │   ├── fonts
│   │   ├── images
│   │   └── style       
│   ├── config
│   ├── tools
│   ├── redux
│   └── components
│       └── App
│       └── Atoms
│       └── Containers
│       └── Molecules
│       └── Organisms
├── package.json
├── .eslintrc
├── .babelrc.js
└── .gitignore
```